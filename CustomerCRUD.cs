﻿using System;
using System.Collections.Generic;
using System.Text;
using Comp3973_Lab1.NW;

namespace Comp3973_Lab1
{
    class CustomerCRUD
    {
        private static NorthwindContext nwContext = new NorthwindContext();

        public static void ViewCustomers()
        {
            Console.WriteLine();

            Console.WriteLine($"{"Customer ID",-20}" +
                              $"{"Contact Name",-40}" +
                              $"{"Company Name",-40}" +
                              $"{"Country\n"}");

            foreach (var customer in nwContext.Customers)
            {
                Console.WriteLine($"{customer.CustomerId,-20}" +
                                  $"{customer.ContactName,-40}" +
                                  $"{customer.CompanyName,-40}" +
                                  $"{customer.Country}");
            }

            Console.WriteLine();
        }

        public static void CreateCustomer(string customerId, string contactName, string companyName, string country)
        {
            var newCustomer = new Customers()
            {
                CustomerId = customerId,
                ContactName = contactName,
                CompanyName = companyName,
                Country = country
            };

            nwContext.Customers.Add(newCustomer);
            nwContext.SaveChanges();
        }

        public static void UpdateCustomer(string customerId, string companyName, string country)
        {
            var customerToUpdate = nwContext
                                   .Customers
                                   .Find(customerId);

            customerToUpdate.CompanyName = companyName;
            customerToUpdate.Country = country;

            nwContext.SaveChanges();
        }

        public static void DeleteCustomer(string customerId)
        {
            var customerToDelete = nwContext
                                   .Customers
                                   .Find(customerId);

            nwContext.Customers.Remove(customerToDelete);
            nwContext.SaveChanges();
        }
    }
}
