﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Comp3973_Lab1
{
    public static class ConsoleUI
    {
        public static void PrintWelcome()
        {
            Console.WriteLine("COMP 3973 Lab 1\n");
        }

        public static void PrintMenu()
        {
            Console.WriteLine("(1) View Customers\n" +
                              "(2) Add Customer\n" +
                              "(3) Edit Customer\n"+
                              "(4) Delete Customer\n" +
                              "(5) Quit\n");
        }
    }
}
