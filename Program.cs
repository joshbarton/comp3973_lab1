﻿using System;
using System.Linq;
using Comp3973_Lab1.NW;

namespace Comp3973_Lab1
{
    class Program
    {
        const int QUIT = 5;

        static void Main(string[] args)
        {
            int input;

            ConsoleUI.PrintWelcome();

            do
            {
                ConsoleUI.PrintMenu();

                input = Convert.ToInt32(Console.ReadLine());

                MenuSelection(input);

            } while (input != QUIT);
        }

        public static void MenuSelection(int selection)
        {
            switch(selection)
            {
                case 1:
                    CustomerCRUD.ViewCustomers();
                    break;
                case 2:
                    InputNewCustomer();
                    break;
                case 3:
                    InputCustomerToUpdate();
                    break;
                case 4:
                    InputCustomerToDelete();
                    break;
                default:
                    Console.WriteLine("\nInvalid input\n");
                    break;
            }
        }

        public static void InputNewCustomer()
        {
            string customerId, contactName, companyName, country;

            Console.WriteLine("\nEnter the Customer ID:");
            customerId = Console.ReadLine();

            Console.WriteLine("\nEnter the Contact name:");
            contactName = Console.ReadLine();

            Console.WriteLine("\nEnter the Company name:");
            companyName = Console.ReadLine();

            Console.WriteLine("\nEnter the Country:");
            country = Console.ReadLine();

            CustomerCRUD.CreateCustomer(customerId, contactName, companyName, country);

            Console.WriteLine("\nCustomer was added.\n");
        }

        public static void InputCustomerToUpdate()
        {
            string customerId, companyName, country;

            Console.WriteLine("\nEnter the Customer's ID:");
            customerId = Console.ReadLine();

            Console.WriteLine("\nEnter the Company Name:");
            companyName = Console.ReadLine();

            Console.WriteLine("\nEnter the Country:");
            country = Console.ReadLine();

            CustomerCRUD.UpdateCustomer(customerId, companyName, country);

            Console.WriteLine("\nCustomer was updated.\n");
        }

        public static void InputCustomerToDelete()
        {
            string customerId;

            Console.WriteLine("\nEnter the Customer's ID:");
            customerId = Console.ReadLine();

            CustomerCRUD.DeleteCustomer(customerId);

            Console.WriteLine("\nCustomer was deleted.\n");
        }
    }
}